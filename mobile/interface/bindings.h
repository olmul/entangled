#ifndef __MOBILE_INTERFACE_BINDINGS_H_
#define __MOBILE_INTERFACE_BINDINGS_H_

#ifdef __cplusplus
extern "C" {
#endif

char* do_pow(const char* trytes, int mwm);

#ifdef __cplusplus
}
#endif

#endif
